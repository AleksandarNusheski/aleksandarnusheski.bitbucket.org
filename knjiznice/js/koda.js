
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


var generatePatients=[
	{fn: 'Brad', ln: 'Pitt', db: '1963-12-18', meritve: [{ds: '2014-05-26', h: 180, w: 86}, {ds: '2014-06-25', h: 180, w: 85}, {ds: '2014-07-20', h: 180, w: 86}, {ds: '2014-08-29', h: 180.1, w: 88}, {ds: '2014-09-23', h: 180.1, w: 89}]},
	{fn: 'Angelina', ln: 'Jolie', db: '1975-06-04', meritve: [{ds: '2014-05-26', h: 169, w: 46}, {ds: '2014-06-25', h: 169, w: 45}, {ds: '2014-07-20', h: 169, w: 49}, {ds: '2014-08-29', h: 169, w: 51}, {ds: '2014-09-23', h: 169, w: 47}]},
	{fn: 'Jennifer', ln: 'Aniston', db: '1969-02-11', meritve: [{ds: '2016-09-03', h: 164, w: 57}, {ds: '2016-09-21', h: 164, w: 58}, {ds: '2016-10-11', h: 164, w: 58}, {ds: '2016-10-28', h: 164, w: 57}, {ds: '2016-11-17', h: 164, w: 58}]}
];


function generateData() {
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 */
function generirajPodatke(stPacienta) {
	stPacienta-=1;
	var ime = generatePatients[stPacienta].fn;
	var priimek = generatePatients[stPacienta].ln;
	var datumRojstva = generatePatients[stPacienta].db;
	
	var sessionId = getSessionId();
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
		url: baseUrl + "/ehr",
		type: 'POST',
		success: function (data) {
			var ehrId = data.ehrId;
			var partyData = {
				firstNames: ime,
				lastNames: priimek,
				dateOfBirth: datumRojstva,
				partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
			};
			$.ajax({
				url: baseUrl + "/demographics/party",
				type: 'POST',
				contentType: 'application/json',
				data: JSON.stringify(partyData),
				success: function (party) {
					if (party.action == 'CREATE') {
						$("#kreirajSporocilo").append("<br/><span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
						$("#preberiObstojeciVitalniZnak").append('<option value="' + ehrId + '">' + ime + " " + priimek +'</option>');
						$("#preberiEhrIdZaVitalneZnake").append('<option value="' + ehrId + '">' + ime + " " + priimek +'</option>');
						
						generatePatients[stPacienta].meritve.forEach(function(el, i){
							var datumInUra = generatePatients[stPacienta].meritve[i].ds;
							var telesnaVisina = generatePatients[stPacienta].meritve[i].h;
							var telesnaTeza = generatePatients[stPacienta].meritve[i].w;
							var podatki = {
								// Struktura predloge je na voljo na naslednjem spletnem naslovu:
								// https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								"ctx/language": "en",
								"ctx/territory": "SI",
								"ctx/time": datumInUra,
								"vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
							};
							var parametriZahteve = {
								ehrId: ehrId,
								templateId: 'Vital Signs',
								format: 'FLAT'
							};
							var sessionId2 = getSessionId();
							$.ajaxSetup({
								headers: {"Ehr-Session": sessionId2}
							});
							$.ajax({
								url: baseUrl + "/composition?" + $.param(parametriZahteve),
								type: 'POST',
								contentType: 'application/json',
								data: JSON.stringify(podatki),
								success: function (res) {
									//$("#dodajMeritveVitalnihZnakovSporocilo").append("<br/><span class='obvestilo label label-success fade-in'>" + res.meta.href.split("https://rest.ehrscape.com/rest/v1/composition/")[1] + "</span>");
								},
								error: function (err) {
									$("#dodajMeritveVitalnihZnakovSporocilo").append("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
								}
							});
						});
					}
				},
				error: function (err) {
					$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
				}
			});
		}
	});
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer izpiši napako.
 */
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 || priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
				id = ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
							$("#preberiObstojeciVitalniZnak").append('<option value="' + ehrId + '">' + ime + " " + priimek +'</option>');
							$("#preberiEhrIdZaVitalneZnake").append('<option value="' + ehrId + '">' + ime + " " + priimek +'</option>');
		                }
		            },
		            error: function (err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno prebran EHR, bolnik '" + party.firstNames + " " + party.lastNames + "', rojen '" + party.dateOfBirth + "'.</span>");
				$("#preberiObstojeciVitalniZnak").append('<option value="' + ehrId + '">' + party.firstNames + " " + party.lastNames +'</option>');
				$("#preberiEhrIdZaVitalneZnake").append('<option value="' + ehrId + '">' + party.firstNames + " " + party.lastNames +'</option>');
			},
			error: function (err) {
				$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena
 * kompozicija, ki vključuje množico meritev vitalnih znakov
 * (EHR ID, datum in ura, telesna višina, telesna teža).
 */
function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();

	if (!ehrId || ehrId.trim().length == 0 || !datumInUra || datumInUra.trim().length == 0 || !telesnaVisina || telesnaVisina.trim().length == 0 || !telesnaTeza || telesnaTeza.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
			// https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-success fade-in'>" + res.meta.href.split("https://rest.ehrscape.com/rest/v1/composition/")[1] + "</span>");
		    },
		    error: function (err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev
 * za vitalnih znakov telesna visina in telesna teža.
 */
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<span>Pridobivanje podatkov bolnika <b>'" + party.firstNames + " " + party.lastNames + "'</b>.</span><br/><br/>");
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + "height",
					type: 'GET',
					headers: {"Ehr-Session": sessionId},
					success: function (resH) {
						if (resH.length > 0) {
							$.ajax({
								url: baseUrl + "/view/" + ehrId + "/" + "weight",
								type: 'GET',
								headers: {"Ehr-Session": sessionId},
								success: function (resW) {
									if (resW.length > 0) {
										var dataProvider=[];
										var results = "<table class='table table-striped table-hover'><tr><th>Datum in ura</th><th class='text-right'>Visina</th><th class='text-right'>Telesna teža</th></tr>";
										for (var i in resW) {
											results += "<tr><td>" + resW[i].time + "</td><td class='text-right'>" + resH[i].height + " " + resH[i].unit + "</td><td class='text-right'>" + resW[i].weight + " " + resW[i].unit + "</td></tr>";
											var timeOnChart=resW[i].time.split("T")[0].split("-");
											var tmp={
												"datumMeritve": timeOnChart[2]+"."+timeOnChart[1]+"."+timeOnChart[0],
												"teza": resW[i].weight,
												"color": "#FF0033"
											};
											dataProvider.push(tmp);
										}
										results += "</table>";
										var bmi = resW[0].weight/(resH[0].height*resH[0].height*0.0001);
										if(bmi<18.5){
											results+="<div class='alert alert-danger' role='alert'>BMI: " + bmi.toFixed(2) + " Zmanjšana telesna masa (nedohranjenost)</div>";
										}else if(bmi>=18.5 && bmi<=25){
											results+="<div class='alert alert-success' role='alert'>BMI: " + bmi.toFixed(2) + " Normalna telesna masa</div>";
										}else if(bmi>25){
											results+="<div class='alert alert-danger' role='alert'>BMI: " + bmi.toFixed(2) + " Zvečana telesna masa (debelost)</div>";
										}
										results+="<button id='legend' type='button' class='btn btn-primary btn-xs' onclick='prikaziLegenda()'>Prikazi legenda</button>";
										$("#rezultatMeritveVitalnihZnakov").append(results);
										
										$("#graficniPrikazRezultatov").html("<div id='chartdiv'></div>");
										var chart = AmCharts.makeChart("chartdiv", {
										  "type": "serial",
										  "theme": "light",
										  "marginRight": 70,
										  "dataProvider": dataProvider,
										  "valueAxes": [{
											"axisAlpha": 0,
											"position": "left",
											"title": "Teza"
										  }],
										  "graphs": [{
											"balloonText": "<b>[[category]]: [[value]]</b>",
											"fillColorsField": "color",
											"fillAlphas": 0.9,
											"lineAlpha": 0.2,
											"type": "line",
											"valueField": "teza"
										  }],
										  "startDuration": 1,
										  "chartCursor": {"categoryBalloonEnabled": false,"cursorAlpha": 0,"zoomable": false},
										  "categoryField": "datumMeritve",
										  "categoryAxis": {"gridPosition": "start","labelRotation": 45},
										  "export": {"enabled": false}
										});
									} else {
										$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>" + "Ni podatkov o visini!</span>");
									}
								},
								error: function() {
									$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
								}
							});
						} else {
							$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>" + "Ni podatkov o tezini!</span>");
						}
					},
					error: function() {
						$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
					}
				});
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}


function prikaziLegenda() {
	$('#legend').css({display: "none", visibility: "hidden"});
	$('#rezultatMeritveVitalnihZnakov').append("<table class='table table-striped table-hover'><tr><th>Kategorija</th><th>BMI</th></tr><tr><td>Nedohranjenost</td><td><18.5</td><tr><td>Normalna telesna masa</td><td>18.5-25</td><tr><td>Debelost</td><td>>25</td></tr>");
};


function prikaziMapa(){
	$('#prikaziMapa').css({display: "none", visibility: "hidden"});
	$('#rezultatMapa').append("<div id='map-canvas'></div>");
	if (navigator.geolocation) navigator.geolocation.getCurrentPosition(showPosition, showError);
	else $('#rezultatMapa').append("Geolocation is not supported by this browser.");
}
var map, infowindow, latitude, longitude;
function showPosition(position) {
	latitude=position.coords.latitude;
	longitude=position.coords.longitude;
	initialize();
}
function showError(error) {
	$('#map-canvas').css({display: "none", visibility: "hidden"});
    switch(error.code) {
        case error.PERMISSION_DENIED: $('#rezultatMapa').append("You have denied the request for Geolocation."); break;
        case error.POSITION_UNAVAILABLE: $('#rezultatMapa').append("Location information is unavailable."); break;
        case error.TIMEOUT: $('#rezultatMapa').append("The request to get user location timed out."); break;
        case error.UNKNOWN_ERROR: $('#rezultatMapa').append("An unknown error occurred."); break;
    }
}
function initialize() {
  var myLocation = new google.maps.LatLng(latitude, longitude);
  map = new google.maps.Map(document.getElementById('map-canvas'), {
	center: myLocation,
	zoom: 13
  });
  var request = {
	location: myLocation,
	radius: 10000,
	types: ['gym']
  };
  infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch(request, function(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
	  createMarkerOnMyLocation();
	    for (var i = 0; i < results.length; i++) {
          createMarker(results[i]);
        }
    }
  });
}
function createMarker(place) {
  var marker = new google.maps.Marker({
	map: map,
	position: place.geometry.location
  });
  google.maps.event.addListener(marker, 'click', function() {
	infowindow.setContent(place.name);
	infowindow.open(map, this);
  });
}
function createMarkerOnMyLocation() {
  var marker = new google.maps.Marker({
	map: map,
	position: new google.maps.LatLng(latitude, longitude)
  });
  google.maps.event.addListener(marker, 'click', function() {
	infowindow.setContent("Your location");
	infowindow.open(map, this);
  });
}

$(document).ready(function() {
  /**
   * Napolni testni EHR ID pri vnosu meritve vitalnih znakov bolnika,
   * ko uporabnik izbere vrednosti iz padajočega menuja (npr. Brad Pitt)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		$("#dodajVitalnoEHR").val($(this).val());
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja (npr. Brad Pitt)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("<p>Trenutno ni rezultatov.</p>");
		$("#graficniPrikazRezultatov").html("<p>Trenutno ni rezultatov.</p>");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
});
