# Delujoča spletna stran:
https://aleksandarnusheski.bitbucket.io

### Opis naloge in navodila ###

Na Bitbucket je na voljo javni repozitorij https://bitbucket.org/dlavbic/dlavbic.bitbucket.org, ki vsebuje ogrodje spletne aplikacije eZdravje (spletna stran začetnega repozitorija je na naslovu https://dlavbic.bitbucket.io).

V okviru domače naloge ustvarite kopijo repozitorija (angl. fork) ter razvijte lastno rešitev spletne aplikacije eZdravje. Pomembno je, da je ime vašega repozitorija {študent}.bitbucket.org, kjer {študent} predstavlja vaše Bitbucket uporabniško ime, saj se vam na ta način samodejno omogoči funkcionalnost Bitbucket Pages, katere povezavo morate tudi oddati v okviru domače naloge.

Celotno nalogo razvijte v veji master.

V okviru 4. domače naloge morate izdelati:

* načrt informacijske rešitve in
* na podlagi načrta implementirati spletno aplikacijo, ki na inovativen način prikaže podatke o vitalnih znakih pacienta (telesna višina, teža, temperatura, sistolični in diastolični krvni tlak ter nasičenost krvi s kisikom) in uporabite:
    * 1 master/detail načrtovalski vzorec,
    * 1 tehniko (graf) vizualizacije podatkov in
    * 1 dodatni zunanji vir podatkov.
Vse zahteve so podrobneje opredeljene v nadaljevanju, v posamičnih nalogah.

Razvita rešitev naj bo spletna aplikacija, ki v celoti deluje na strani odjemalca in s pomočjo API klicev pridobiva podatke iz zunanjih virov (EhrScape in 1 zunanji vir, ki ga izberete sami).

Delovanje na strani odjemalca pomeni, da v primeru, če si izvorno kodo vaše aplikacije prenesemo na svoj lokalni računalnik in zgolj odpremo datoteko index.html v spletnem brskalniku, mora aplikacija delovati, če ima računalnik dostop do interneta.

### Opis osnovne ideje predloga ###

Za motivacijo pri iskanju ideje si poglejte demo aplikacije na EhrScape platformi, na voljo pa je tudi njihova izvorna koda. Predlog rešitve naj bo vaš, kjer si morate zamisliti scenarij uporabe, končnega uporabnika (pacient) in namen uporabe (npr. preventiva, obveščenost ipd.).

Aplikacija naj ne uporablja lastne podatkovne baze, ampak naj zgolj dostopa (bere in ažurira) do podatkov iz naslednjih podatkovnih virov:
* podatki o vitalnih znakih (ni potrebno uporabiti vseh) iz platforme EhrScape s pomočjo Electronich Health Record API,
* 1 zunanji podatkovni vir (npr. WHO, Zdravniška zbornica, Zdravniki.org, Lekarnar, NIJZ, Statistični urad RS, Združenje zdravstvenih zavodov Slovenije, Drugs.com, HealthMap, Twitter, Facebook, DuckDuckGo Instant Answer, Google Maps, GeoNames idr.)

### Generiranje podatkov ###

Za potrebe testiranja v podatkovni bazi EhrScape pripravite tudi 3 vzorčne paciente (npr. pacient, ki je kronično bolan in ima posledično slabe vitalne znake; pacient, ki je športnik in ima idealne vitalne znake; otrok, ki se poškoduje in njegovi vitalni znaki nakazujejo poškodbo; starejša oseba, ki je hospitalizirana zaradi pljučnice ipd.), ki bi vašo aplikacijo uporabljali in za njih napolnite podatke (t.j. v EhrScape podatkovno bazo vstavite nekaj testnih podatkov za vsakega pacienta).

Omenjene vzorčne paciente naj bo mogoče izbrati iz menuja, ko se začne uporabljati vašo aplikacijo. Omogočen pa naj bo tudi vnos EhrID, tako da lahko vašo aplikacijo uporablja poljuben pacient.

### Načrt informacijske rešitve ###

Za delovanje vaše aplikacije morate pripraviti načrt informacijske rešitve po objektnem pristopu. Uporabiti morate UML diagramsko tehniko in sicer lahko diagram narišete na papir in ga fotografirate ali ga izvozite kot sliko iz programa Lucid Chart ali kateregakoli drugega programa za načrtovanje informacijskih sistemov.

Diagrami, ki jih boste pripravili v okviru načrta, morajo biti med seboj skladni, kar pomeni:

* diagram primerov uporabe (DPU) mora predstavljati vse funkcionalnosti, ki jih vaša aplikacija ponuja,
* VOPC razredni diagram mora vsebovati vse atribute (t.j. vnosna polja, globalne spremenljivke idr.) in metode (t.j. akcije nad elementi, npr. kliki gumbov ipd.) vaše predlagane aplikacije,
* izbrani diagram zaporedja (pripravite zgolj enega za osnovni tok ene izmed vaših glavnih funkcionalnosti) mora realizirati izbran primer uporabe in skupaj z VOPC razrednim diagramom mora biti dovolj podroben, da lahko programer to funkcionalnost tudi implementira.
Zato bodite še posebej pozorni, da bo načrt skladen z vašo implementacijo!

### Implementacija aplikacije ###

Pri implementaciji upoštevajte naslednje zahteve in omejitve:

* implementirajte inovativno aplikacijo, ki deluje nad podatki o vitalnih znakih pacienta (telesna višina, teža, temperatura, sistolični in diastolični krvni tlak ter nasičenost krvi s kisikom, kjer vam ni potrebno uporabiti vseh) iz platforme EhrScape in 1 zunanjim virom podatkov,
* izvajanje vaše aplikacije se lahko začne na 2 načina:
    * uporabnik naj ima možnost izbora ene izmed 3 vzorčnih oseb iz padajočega menuja,
    * na voljo naj bo generator podatkov, ki omogoča ponovni zagon generiranja podatkov,
* uporabite 1 master/detail načrtovalski vzorec
    * t.j. funkcionalnost, ko uporabnik izbere določeno vrednost iz seznama, se naj posodobi množica odvisnih podatkov (npr. podobno, kot smo implementirali pregled albumov, seznamov predvajanj in žanrov po izvajalcih na vajah V5),
* pri vizualizaciji podatkov uporabite 1 tehniko, ki je na voljo v knjižnici [D3.js](https://d3js.org) ([primeri](https://github.com/d3/d3/wiki/Gallery)) ali kakšni drugi poljubni JavaScript knjižnici za vizualizacijo.